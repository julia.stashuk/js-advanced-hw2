//Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.
//try...catch робить наш код більш захищеним і якщо виникає будь-яка помилка дозволяє її швидше та гнучкіше обробляти.
//try...catch доречно використовувати при роботі з зовнішніми ресурсами, такими як бази даних, API, файлові системи та при роботі з динамічними данними,
//які можуть змінюватися. try...catch використовується для перехоплення та обробки помилок при таких зміннах, невірно вказаних шляхах до файлів, введенні 
//даних в хибному форматі тощо, і виконання відповідних дій.

const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

  function createBookList() {
    const rootElement = document.getElementById("root");
    const ul = document.createElement("ul");
    
    books.forEach(book => {
      try {
          if (!book.author || !book.name || !book.price) {
              throw new Error(`Помилка: Не вказані всі необхідні властивості: ${JSON.stringify(book)}`);
          } 
            const li = document.createElement("li");
            li.textContent = `Автор: ${book.author}, Назва: ${book.name}, Ціна: ${book.price}`;
            ul.appendChild(li);
          } catch (error) {
            console.error(error.message);
        }
    });

  rootElement.appendChild(ul);
  return ul;
}

createBookList();
